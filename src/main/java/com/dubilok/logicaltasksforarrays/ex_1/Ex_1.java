package com.dubilok.logicaltasksforarrays.ex_1;

import java.util.Arrays;

public class Ex_1 {

    public static void main(String[] args) {
        int [] first = new int[100000];
        for (int i = 0; i < first.length; i++) {
            first[i] = i;
        }
        int [] second = new int[100000];
        for (int i = 0; i < second.length; i++) {
            first[i] = i;
        }
        long st = System.currentTimeMillis();
        int third[] = containsElementInTwoArrays(first, second);
        long fn = System.currentTimeMillis();
        System.out.println(fn - st);
    }

    public static int[] containsElementInTwoArrays(int[] first, int[] second) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                if (first[i] == second[j]) {
                    str.append(first[i]);
                    str.append(" ");
                    break;
                }
            }
        }
        return getArrayFromStr(str.toString());
    }

    public static int[] containsElementOnlyInOneArray(int[] first, int[] second) {
        String str = "";
        Arrays.sort(first);
        Arrays.sort(second);
        str += getAllElementsFirst(first, second);
        str += getAllElementsFirst(second, first);
        return getArrayFromStr(str);
    }

    public static int[] getArrayFromStr(String str) {
        if (!str.isEmpty()) {
            String[] strArray = str.split(" ");
            int[] result = new int[strArray.length];
            for (int i = 0; i < strArray.length; i++) {
                result[i] = Integer.parseInt(strArray[i]);
            }
            return result;
        }
        return new int[]{};
    }

    private static String getAllElementsFirst(int first[], int[] second) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < first.length; i++) {
            boolean flag = false;
            for (int j = 0; j < second.length; j++) {
                if (first[i] == second[j]) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                str.append(first[i]);
                str.append(" ");
            }
        }
        return str.toString();
    }

}
