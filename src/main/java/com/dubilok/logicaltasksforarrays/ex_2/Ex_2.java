package com.dubilok.logicaltasksforarrays.ex_2;

import com.dubilok.logicaltasksforarrays.ex_1.Ex_1;

import java.util.Arrays;

public class Ex_2 {

    public static void main(String[] args) {
        int[] mas = {1,2,2,2,3,4,4,3,3,5,6,7};
        int[] res = deleteAllElementsContainsMoreThanTwoTimes(mas);
        System.out.println(Arrays.toString(res));
    }

    public static int[] deleteAllElementsContainsMoreThanTwoTimes(int[] mas) {
        Arrays.sort(mas);
        StringBuilder str = new StringBuilder();
        countRepeatOfNumber(mas, str);
        return Ex_1.getArrayFromStr(str.toString());
    }

    private static void countRepeatOfNumber(int mas[], StringBuilder str) {
        for (Integer i : mas) {
            long countEl = Arrays.stream(mas).filter(k -> k == i).count();
            if (countEl <= 2) {
                str.append(i).append(" ");
            }
        }
    }
}
