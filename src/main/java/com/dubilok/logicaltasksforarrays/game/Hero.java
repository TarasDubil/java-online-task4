package com.dubilok.logicaltasksforarrays.game;

public class Hero {

    private String name;
    private int strength;
    private boolean isAlive;
    private boolean sWonTheGame;

    public Hero() {
        this.strength = 25;
        this.isAlive = true;
    }

    public Hero(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public boolean issWonTheGame() {
        return sWonTheGame;
    }

    public void setsWonTheGame(boolean sWonTheGame) {
        this.sWonTheGame = sWonTheGame;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", strength=" + strength +
                ", isAlive=" + isAlive +
                '}';
    }
}
