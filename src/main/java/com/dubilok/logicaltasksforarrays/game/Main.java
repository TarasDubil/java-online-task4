package com.dubilok.logicaltasksforarrays.game;

import com.dubilok.logicaltasksforarrays.game.console.CommandMenu;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        CommandMenu commandMenu = new CommandMenu();
        try {
            commandMenu.run(new Area());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
