package com.dubilok.logicaltasksforarrays.game;

import com.dubilok.logicaltasksforarrays.ex_1.Ex_1;
import com.dubilok.logicaltasksforarrays.game.prize.Artifact;
import com.dubilok.logicaltasksforarrays.game.prize.Monster;

public class Area {

    private Hero hero;
    private Door[] doors = new Door[10];
    private int countOfDoorsOfDeath;
    private int countOpenDoor;

    public Area() {
        initializeDoor();
        hero = new Hero();
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public Door[] getDoors() {
        return doors;
    }

    public void setDoors(Door[] doors) {
        this.doors = doors;
    }

    public int getCountOfDoorsOfDeath() {
        return countOfDoorsOfDeath;
    }

    public void setCountOfDoorsOfDeath(int countOfDoorsOfDeath) {
        this.countOfDoorsOfDeath = countOfDoorsOfDeath;
    }

    public int getCountOpenDoor() {
        return countOpenDoor;
    }

    public void setCountOpenDoor(int countOpenDoor) {
        this.countOpenDoor = countOpenDoor;
    }

    private void initializeDoor() {
        for (int i = 0; i < 10; i++) {
            doors[i] = new Door();
        }
    }

    public void battle(Hero hero, Monster monster) {
        if (hero.getStrength() >= monster.getStrength()) {
            monster.setAlive(false);
        } else {
            hero.setAlive(false);
        }
    }

    public void openDoorInOrder() throws DoorException {
        int i = 0;
        while (hero.isAlive() && i < 10) {
            openDoorByNumber(i);
            i++;
        }
        if (hero.isAlive()) {
            hero.setsWonTheGame(true);
            System.out.println("Hero was opened all doors and has won the game!!!");
        } else {
            hero.setsWonTheGame(false);
            System.out.println("Hero was opened [" + (i+1) + "] doors and has lost the game!");
        }
    }

    public void openDoorByNumber(int number) throws DoorException {
        if ((number > doors.length) || (number < 0)) {
            throw new DoorException("You have entered illegal argument exception " + number);
        }
        if (hero.issWonTheGame()) {
            return;
        }
        doors[number].setOpen(true);
        countOpenDoor++;
        if (doors[number].getPrize() instanceof Artifact) {
            hero.setStrength(hero.getStrength() + doors[number].getPrize().getStrength());
        } else {
            battle(hero, (Monster) doors[number].getPrize());
        }
        if (hero.isAlive()) {
            System.out.println("Hero was opened [" + number + "] door and has won the battle!");
        } else {
            System.out.println("Hero was opened [" + number + "] door and has lost the battle!");
            return;
        }
        if (!hero.isAlive()) {
            System.out.println("Hero has lost the game");
        }
        if (countOpenDoor == doors.length) {
            hero.setsWonTheGame(true);
        }
    }

    //Information about Doors
    public void getInfoAboutDoor() {
        for (int i = 0; i < doors.length; i++) {
            System.out.println("Door[" + i + "]" + doors[i]);
        }
    }

    //Count death recursion
    public void getCountDeathOfHero(Door[] doors, int i) {
        if (doors[i++].getPrize() instanceof Monster) {
            countOfDoorsOfDeath++;
        }
        if (i < doors.length) {
            getCountDeathOfHero(doors, i);
        }
    }

    public int[] openDoorAndBeAlive() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < doors.length; i++) {
            if ((doors[i].getPrize() instanceof Artifact) || (hero.getStrength() >= doors[i].getPrize().getStrength())) {
                stringBuilder.append(i).append(" ");
            }
        }
        String str = stringBuilder.toString();
        int mas[] = Ex_1.getArrayFromStr(str);
        return mas;
    }
}
