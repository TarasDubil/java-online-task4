package com.dubilok.logicaltasksforarrays.game.console;

import com.dubilok.logicaltasksforarrays.game.Area;
import com.dubilok.logicaltasksforarrays.game.DoorException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class CommandMenu {

    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public void run(Area area) throws IOException {
        while (true) {
            System.out.println("if you want to open door by standart method enter: 'open st'\n" +
                    "if you want open door and enter your number for open enter: 'open num'\n" +
                    "If you want to know information what is behind the door enter: 'infa'\n" +
                    "if you want to count at how many doors the hero is waiting for death enter: 'count'\n" +
                    "Output the door numbers in the order in which they should be opened to the hero to stay alive enter " +
                    "'number'\nIf you want to exit enter 'exit'");
            String message = bufferedReader.readLine();
            if (message.equals(Menu.OPEND_DOOR_IN_ORDER.getMessage())) {
                try {
                    area.openDoorInOrder();
                } catch (DoorException e) {
                    e.printStackTrace();
                }
            } else if (message.equals(Menu.OPEN_DOOR_BY_NUMBER.getMessage())) {
                openDoorByNumber(area);
            }
            else if (message.equals(Menu.INFA.getMessage())) {
                area.getInfoAboutDoor();
            } else if (message.equals(Menu.COUNT.getMessage())) {
                area.getCountDeathOfHero(area.getDoors(), 0);
                System.out.println(area.getCountOfDoorsOfDeath());
            } else if (message.equals(Menu.NUMBER.getMessage())) {
                int[] openDoorAndBeAlive = area.openDoorAndBeAlive();
                System.out.println("You need to open door in that order!");
                System.out.println(Arrays.toString(openDoorAndBeAlive));
            } else if (message.equals(Menu.EXIT.getMessage())) {
                System.out.println("End!");
                break;
            } else {
                System.out.println("---------------------------\nRepeat yor choise!");
            }
            System.out.println("\n-----------------------------\n");
        }
        bufferedReader.close();
    }

    private void openDoorByNumber(Area area) throws IOException {
        while (true) {
            if (area.getHero().issWonTheGame()) {
                System.out.println("Hero won the game!");
                return;
            }
            if (!area.getHero().isAlive()) {
                System.out.println("Hero lost the game!");
                return;
            }
            System.out.println("Enter your number of door!");
            int number = Integer.parseInt(bufferedReader.readLine());
            while (true) {
                try {
                    area.openDoorByNumber(number);
                    break;
                } catch (DoorException e) {
                    System.out.println("Repeat your choise!");
                    number = Integer.parseInt(bufferedReader.readLine());
                }
            }
        }
    }
}
