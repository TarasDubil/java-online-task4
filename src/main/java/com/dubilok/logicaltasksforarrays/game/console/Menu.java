package com.dubilok.logicaltasksforarrays.game.console;

public enum Menu {
    OPEND_DOOR_IN_ORDER("open st"),
    OPEN_DOOR_BY_NUMBER("open num"),
    INFA("infa"),
    COUNT("count"),
    NUMBER("number"),
    EXIT("exit");

    private final String message;

    Menu(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
