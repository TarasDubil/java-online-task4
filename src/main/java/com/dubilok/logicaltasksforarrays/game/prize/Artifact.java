package com.dubilok.logicaltasksforarrays.game.prize;

import java.util.Random;

public class Artifact implements Prize {

    private int strength;

    public Artifact() {
        this.strength = new Random().nextInt(81-10) + 10;
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "strength=" + strength +
                '}';
    }
}
