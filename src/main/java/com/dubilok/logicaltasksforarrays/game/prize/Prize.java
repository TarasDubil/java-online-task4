package com.dubilok.logicaltasksforarrays.game.prize;

public interface Prize {

    int getStrength();

}
