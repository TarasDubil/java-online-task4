package com.dubilok.logicaltasksforarrays.game.prize;

import java.util.Random;

public class Monster implements Prize {

    private int strength;
    private boolean isAlive;

    public Monster() {
        this.strength = new Random().nextInt(101-5) + 5;
        this.isAlive = true;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "strength=" + strength +
                '}';
    }
}
