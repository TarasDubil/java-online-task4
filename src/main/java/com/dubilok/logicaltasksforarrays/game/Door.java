package com.dubilok.logicaltasksforarrays.game;

import com.dubilok.logicaltasksforarrays.game.prize.Artifact;
import com.dubilok.logicaltasksforarrays.game.prize.Monster;
import com.dubilok.logicaltasksforarrays.game.prize.Prize;

import java.util.Random;

public class Door {

    private boolean isOpen;
    private Prize prize;

    public Door() {
        isOpen = false;
        initialize();
    }

    private void initialize() {
        int number = new Random().nextInt(101-1) + 1;
        if (number % 2 == 0) {
            prize = new Artifact();
        } else {
            prize = new Monster();
        }
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public Prize getPrize() {
        return prize;
    }

    public void setPrize(Prize prize) {
        this.prize = prize;
    }

    @Override
    public String toString() {
        return "[isOpen=" + isOpen +
                ", prize=" + prize +
                '}';
    }
}
