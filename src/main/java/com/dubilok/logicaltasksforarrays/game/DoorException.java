package com.dubilok.logicaltasksforarrays.game;

public class DoorException extends Exception {

    public DoorException() {
    }

    public DoorException(String message) {
        super(message);
    }

    public DoorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DoorException(Throwable cause) {
        super(cause);
    }

    public DoorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
