package com.dubilok.logicaltasksforarrays.ex_3;

import com.dubilok.logicaltasksforarrays.ex_1.Ex_1;
import java.util.Arrays;

public class Ex_3 {

    public static void main(String[] args) {
        int mas[] = getArrayWithOutRepeatNumber(new int[]{1, 7, 1, 2, 2, 0, 2, 3, 4, 4, 5, -3, 5, 6, 6, 6});
        System.out.println(Arrays.toString(mas));
    }

    public static int[] getArrayWithOutRepeatNumber(int mas[]) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < mas.length-1; i++) {
            if (mas[i] != mas[i + 1]) {
                stringBuilder.append(mas[i]).append(" ");
            } else {
                stringBuilder.append(mas[i]).append(" ");
                i++;
            }

        }
        return Ex_1.getArrayFromStr(stringBuilder.toString());
    }

}
