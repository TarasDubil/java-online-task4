package com.dubilok.learnby.optionaltask.ex_8;

import java.io.*;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        try(FileInputStream fileInputStream = new FileInputStream("C:\\Users\\Z30\\Desktop\\test.txt")) {
            String dataFromFile = getDataFromFile(fileInputStream);
            HashSet<String> set = getOnlyEnglishLetters(dataFromFile);
            set.forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getDataFromFile(FileInputStream fis) throws IOException {
        String str = "";
        while (fis.available() > 0) {
            str += (char) fis.read();
        }
        return str;
    }

    public static HashSet<String> getOnlyEnglishLetters(String str) {
        String strArray[] = str.split("[ \n]");
        HashSet<String> set = new HashSet<>();
        for (int i = 0; i < strArray.length; i++) {
            boolean flag = true;
            for (int j = 0; j < strArray[i].length(); j++) {
                int letter = strArray[i].charAt(j);
                flag = isLetter(letter);
            }
            if (flag) {
                set.add(strArray[i].toLowerCase());
            }
        }
        return set;
    }

    private static boolean isLetter(int letter) {
        if (!(((letter >= 65) && (letter <=90)) || ((letter >= 97) && (letter <=122)))) {
            return false;
        }
        return true;
    }

}
