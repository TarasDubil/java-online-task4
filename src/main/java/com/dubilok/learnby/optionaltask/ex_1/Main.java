package com.dubilok.learnby.optionaltask.ex_1;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                new FileInputStream("C:\\Users\\Z30\\Desktop\\test.txt")));
            BufferedWriter bufferedWriter = new BufferedWriter(
                    new FileWriter("C:\\Users\\Z30\\Desktop\\test2.txt"))) {
            ArrayList<String> list = getDataFromFile(bufferedReader);
            Collections.reverse(list);
            writeDataToFileFromList(list, bufferedWriter);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getDataFromFile(BufferedReader buf) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        while (buf.ready()) {
            list.add(buf.readLine());
        }
        return list;
    }

    public static void writeDataToFileFromList(ArrayList<String> list, BufferedWriter buf) {
        list.forEach(k -> {
            try {
                buf.write(k);
                buf.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
