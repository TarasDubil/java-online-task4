package com.dubilok.learnby.optionaltask.ex_5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Main {

    private static ArrayList<Integer> list = new ArrayList<>();

    static {
        list.addAll(Arrays.asList(1,2,-3,-8,3,7,5,-4,0));
    }

    public static void main(String[] args) {
        Collections.sort(list);
        Collections.reverse(list);
        list.forEach(System.out::println);
    }

}
