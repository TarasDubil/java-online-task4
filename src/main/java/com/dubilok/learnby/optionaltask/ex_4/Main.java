package com.dubilok.learnby.optionaltask.ex_4;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<String> list = createList();
        list.stream().sorted(Comparator.comparing(String::length)).collect(Collectors.toList());
    }

    public static List<String> createList() {
        ArrayList<String> list = new ArrayList<>();
        String str = "When the band began to pladsaffffffffffffffffffffffy\n" +
                "the sun was shinin' bright.\n" +
                "Now the milkman's on his way,\n" +
                "it's too late to say goodnight.";
        String[] strArray = str.split("\n");
        list.addAll(Arrays.asList(strArray));
        return list;
    }



}
