package com.dubilok.learnby.optionaltask.ex_6;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                new FileInputStream("C:\\Users\\Z30\\Desktop\\test.txt")))) {
            ArrayList<String> list = getDataFromFile(bufferedReader);
            Collections.sort(list);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getDataFromFile(BufferedReader buf) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        while (buf.ready()) {
            list.add(buf.readLine());
        }
        return list;
    }

}
