package com.dubilok.collectionstask.stringmap;

import java.util.Comparator;

public class StringMapComparatorBySecondString implements Comparator<StringMap> {
    @Override
    public int compare(StringMap o1, StringMap o2) {
        return o1.getSecond().compareTo(o2.getSecond());
    }
}
