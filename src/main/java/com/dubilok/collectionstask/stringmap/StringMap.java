package com.dubilok.collectionstask.stringmap;

import java.util.Objects;

public class StringMap implements Comparable<StringMap> {

    private String first;
    private String second;

    public StringMap(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return "StringMap{" +
                "first='" + first + '\'' +
                ", stringmap='" + second + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        StringMap stringMap = (StringMap) object;
        return first.equals(stringMap.first) &&
                second.equals(stringMap.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    @Override
    public int compareTo(StringMap o) {
        return o.first.compareTo(first);
    }
}