package com.dubilok.collectionstask.stringmap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Test {

    private static ArrayList<StringMap> list = new ArrayList<>();
    private static StringMap[] array = {new StringMap("Danulov", "Nazar"), new StringMap("Pex", "Oleg")};

    static  {
        list.add(new StringMap("Dubil", "Taras"));
        list.add(new StringMap("Danulov", "Nazar"));
        list.add(new StringMap("Pex", "Oleg"));
    }

    public static void main(String[] args) {
        System.out.println("Comparing by first String: ");
        list.forEach(System.out::println);
        Collections.sort(list);
        System.out.println();
        list.forEach(System.out::println);
        System.out.println("------------------------\nComparing by Second String!");
        list.forEach(System.out::println);
        Collections.sort(list, new StringMapComparatorBySecondString());
        System.out.println();
        list.forEach(System.out::println);
        System.out.println("-------------------------\nBinary search in list!");
        int indexInList = Collections.binarySearch(list, new  StringMap(null, "Oleg"),
                new StringMapComparatorBySecondString());
        System.out.println(indexInList);
        System.out.println("-------------------------\nBinary search in array!");
        int indexInArray = Arrays.binarySearch(array, new  StringMap(null, "Nazar"),
                new StringMapComparatorBySecondString());
        System.out.println(indexInArray);
    }

}
