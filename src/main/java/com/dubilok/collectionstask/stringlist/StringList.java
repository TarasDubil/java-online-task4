package com.dubilok.collectionstask.stringlist;

public class StringList {

    private static final int DEFAULT_CAPACITY = 10;

    private int size = 0;

    private String[] array;

    public StringList() {
        this.array = new String[DEFAULT_CAPACITY];
    }

    public StringList(int initialCapacity) {
        if (initialCapacity > 0) {
            this.array = new String[initialCapacity];
        } else if (initialCapacity == 0) {
            this.array = new String[]{};
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
    }

    public boolean add(String element) {
        if (size == array.length) {
            String[] moreArray = getNewMoreArray();
            copyArray(this.array, moreArray);
            moreArray[size++] = element;
            this.array = moreArray;
        } else {
            this.array[size++] = element;
        }
        return true;
    }

    public void add(Object o) {
        add(String.valueOf(o));
    }

    public String get(int position) {
        if ((position < 0) || (position > array.length-1)) {
            throw new IllegalArgumentException("Illegal position: " + position);
        }
        return this.array[position];
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private void copyArray(String[] from, String[] to) {
        for (int i = 0; i < from.length; i++) {
            to[i] = from[i];
        }
    }

    private String[] getNewMoreArray() {
        return new String[(int) (this.array.length * 1.5 + 1)];
    }
}
