package com.dubilok.collectionstask.stringlist;

import java.util.*;

public class TestStringList {

    public static void main(String[] args) {
        StringList stringList = new StringList();
        ArrayList<String> arrayList = new ArrayList<>();

        long startAddToStringList = System.currentTimeMillis();
        addElements(stringList);
        long finishAddToStringList = System.currentTimeMillis();
        System.out.println("Time to add 10_000 elements to StringList = " + (finishAddToStringList - startAddToStringList));
        long startAddToArrayList = System.currentTimeMillis();
        addElements(arrayList);
        long finishAddToArrayList = System.currentTimeMillis();
        System.out.println("Time to add 10_000 elements to ArrayList = " + (finishAddToArrayList - startAddToArrayList));

        System.out.println("---------------------------------------------------------");

        long startGetToStringList = System.currentTimeMillis();
        getElements(stringList);
        long finishGetToStringList = System.currentTimeMillis();
        System.out.println("Time to get 10_000 elements from StringList = " + (finishGetToStringList - startGetToStringList));
        long startGetToArrayList = System.currentTimeMillis();
        getElements(arrayList);
        long finishGetToArrayList = System.currentTimeMillis();
        System.out.println("Time to get 10_000 elements from ArrayList = " + (finishGetToArrayList - startGetToArrayList));
    }

    public static void getElements(List<String> list) {
        for (int i = 0; i < 1_000_000; i++) {
            list.get(i);
        }
    }

    public static void getElements(StringList list) {
        for (int i = 0; i < 1_000_000; i++) {
            list.get(i);
        }
    }

    public static void addElements(List<String> list) {
        for (int i = 0; i < 1_000_000; i++) {
            list.add(String.valueOf(i));
        }
    }

    public static void addElements(StringList list) {
        for (int i = 0; i < 1_000_000; i++) {
            list.add(true);
        }
    }
}
